import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import StackNavigation from "./src/Navigations/StackNavigation";
import { AuthProvider } from "./src/Context/AuthContext";

const App = () => {
  return (
    <AuthProvider>
      <NavigationContainer>
        <StackNavigation />
      </NavigationContainer>
    </AuthProvider>
  )
};

export default App;