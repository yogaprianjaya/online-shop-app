export { default as AccountPNG } from './account.png';
export { default as CartPNG } from './cart.png';
export { default as HomePNG } from './home.png';
export { default as SanbercodePNG } from './sanbercode.png';