import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

const Header = (props) => {
    const { dataUser } = props;
    return(
        <View style={header.container}>
            <View style={header.avatarWrapper}>
                <View style={header.avatar}>
                    <Icon name="person" color='#fff' size={50} />
                    <Text style={header.avatarText}>{dataUser.name.firstname}</Text>
                </View>
            </View>
            <View style={header.detailWrapper}>
                <Text style={header.detailName}>{dataUser.name.firstname.toUpperCase()} {dataUser.name.lastname.toUpperCase()}</Text>
                <View style={styling.flexRow}>
                    <Icon name="medal" color='gold' size={18} />
                    <Text style={header.detailMedal}>Gold Member</Text>
                </View>
                <View style={styling.flexRow}>
                    <Text style={header.detailPointText}>With</Text>
                    <Text style={header.detailPoint}>3000</Text>
                    <Text style={header.detailPointText}>point</Text>
                </View>
            </View>
        </View>
    );
};

export default Header;

const styling = StyleSheet.create({
    flexRow: {flexDirection: 'row'},
})

const header = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: '#fff', 
        marginTop: 20, 
        marginHorizontal: 30,
        borderWidth: 1,
        borderColor: '#9087F4',
        borderRadius: 20,
        shadowColor: '#171717',
        elevation: 10,
    },
    avatarWrapper: {width: '40%', padding: 10},
    avatar: {height: 100, borderRadius: 20, backgroundColor: '#9087F4', alignItems: 'center', justifyContent: 'center'},
    avatarText: {color: '#fff'},
    detailWrapper: {width: '60%', justifyContent: 'center', paddingHorizontal: 10},
    detailName: {fontSize: 20, fontWeight: 'bold'},
    detailMedal: {color: 'gold', fontSize: 16, marginLeft: 3},
    detailPointText: {fontSize: 11},
    detailPoint: {fontSize: 11, marginHorizontal: 3, fontWeight: 'bold'},
});