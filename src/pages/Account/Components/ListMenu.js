import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

const ListMenu = (props) => {
    return(
        <View key={props.index} style={menu.container}>
            <Text style={menu.title}>{props.data.title}</Text>
            {props.data.listMenu.map((item, index) => (
                <View style={menu.wrapper} key={index}>
                    <View style={menu.flexWrapper}>
                        <Icon name={item.icon} size={20} />
                        <Text style={menu.list}>{item.title}</Text>
                    </View>
                    <Icon name="chevron-forward" size={20} />
                </View>
            ))}
        </View>
    )
};

export default ListMenu;

const menu = StyleSheet.create({
    container: {marginTop: 20},
    title: {fontSize: 18, fontWeight: 'bold'},
    wrapper: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginLeft: 20, 
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#9087F4',
    },
    flexWrapper: {flexDirection: 'row'},
    list: {fontSize: 17, marginTop: 1, marginLeft: 10},
})