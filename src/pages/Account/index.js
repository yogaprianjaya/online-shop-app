import React, { useContext, useEffect, useState } from "react";
import { Pressable, ScrollView, StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import { get } from "../../Helper/Api";
import Loading from '../../Components/Loading';
import { Header, ListMenu } from "./Components";
import { AuthContext } from "../../Context/AuthContext";

const Account = () => {
    const [dataUser, setDataUser] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [token, setToken] = useContext(AuthContext);
    const menu = [
        { 
            title: 'User Account', 
            listMenu: [
                {icon: 'man', title: 'Detail User'},
                {icon: 'lock-closed', title: 'Reset Password'}
            ]
        },
        { 
            title: 'About', 
            listMenu: [
                {icon: 'people', title: 'About Us'},
                {icon: 'logo-android', title: 'App Description'},
                {icon: 'star', title: 'Rate This App'},
            ]
        },
    ];

    const getDataUser = async () => {
        const response = await get(`/users/1`);
        if(response) {
            setDataUser(response);
            setIsLoading(false);
        }
    };

    useEffect(() => {
        getDataUser();
    }, []);

    const HandlerLogout = () => {
        setToken(null);
    };

    if(isLoading) {
        return(
            <View style={loading.container}>
                <Loading />
            </View>
        )
    }
    else {
        return (
            <View style={wrapper.container}>
                <View style={styling.fullWidth}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={wrapper.body}>
                            <View style={wrapper.content}> 
                                <Header dataUser={dataUser} />

                                <View style={{marginTop: 10, marginHorizontal: 15}}>
                                    {menu.map((item, index) => (
                                        <ListMenu data={item} index={index} key={index} />
                                    ))}
                                </View>

                                <View style={logout.wrapper}>
                                    <Pressable style={styling.flexRow} onPress={HandlerLogout}>
                                        <Icon name="log-out-outline" color='#fff' size={18} />
                                        <Text style={logout.text}>Logout</Text>
                                    </Pressable>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
};

export default Account;

const styling = StyleSheet.create({
    fullWidth: {width: '100%'},
    flexRow: {flexDirection: 'row'},
});

const loading = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
})

const wrapper = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', backgroundColor: '#fff'},
    body: {backgroundColor: '#9087F4', paddingTop: 20},
    content: {
        width: '100%',
        backgroundColor: '#fff',
        paddingHorizontal: 5,
        paddingTop: 10,
        paddingBottom: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
});

const logout = StyleSheet.create({
    wrapper: {
        marginTop: 45, 
        marginHorizontal: 35,
        paddingVertical: 10, 
        alignItems: 'center',
        backgroundColor: '#9087F4',
        borderRadius: 15,
        shadowColor: '#171717',
        elevation: 10,
    },
    text: {fontSize: 15, fontWeight: 'bold', color: '#fff', marginLeft: 5},
});