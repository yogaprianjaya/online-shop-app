import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";

import Loading from "../../Components/Loading";
import { get } from "../../Helper/Api";
import { Product } from "./Components";

const ProductCategory = ({navigation, route}) => {
    const {data} = route.params;
    const [dataProduct, setDataProduct] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const getDataProduct = async () => {
        const response = await get(`/products/category/${data}`);
        if(response) {
            setIsLoading(false);
            setDataProduct(response);
        }
    };

    useEffect(() => {
        getDataProduct();
    }, [data]);

    if(isLoading) {
        return(
            <View style={loading.container}>
                <Loading />
            </View>
        )
    }
    else {
        return (
            <View style={wrapper.container}>
                <View style={styling.widthFull}>
                    <View style={wrapper.body}>
                        <View style={wrapper.content}> 
                            <FlatList 
                                data={dataProduct}
                                keyExtractor={item => item.id}
                                numColumns={2}
                                columnWrapperStyle={{justifyContent: 'space-between'}}
                                renderItem={({item}) => <Product item={item} navigation={navigation}/>}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
};

export default ProductCategory;

const styling = StyleSheet.create({
    widthFull: {width: '100%'},
});

const loading = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
})

const wrapper = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', backgroundColor: '#fff'},
    body: {
        backgroundColor: '#9087F4',
        paddingTop: 20,
    },
    content: {
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
});