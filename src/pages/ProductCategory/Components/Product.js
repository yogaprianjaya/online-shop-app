import React from "react";
import { Dimensions, Image, Pressable, StyleSheet, Text, View } from "react-native";


const Product = (props) => {
    const {item, navigation} = props;
    return(
        <View style={product.container}>
            <View style={product.imageWrapper}>
                <Image source={{uri: item.image}} style={product.image} />
            </View>
            <Text style={product.title}>{item.title}</Text>
            <Text style={product.price}>Price : $ {item.price}</Text>
            <View style={button.container}>
                <Pressable style={button.wrapper} onPress={() => navigation.navigate('ProductDetail', {data: item.id})}>
                    <Text style={button.text}>Detail Product</Text>
                </Pressable>
            </View>
        </View>
    )
};

export default Product;

const screenWidth = Dimensions.get('screen').width - 40;

const product = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        shadowColor: '#171717',
        elevation: 10,
        borderColor: '#9087F4', 
        borderWidth: 1, 
        width: screenWidth / 2 - 10, 
        paddingHorizontal: 5, 
        paddingTop: 7, 
        paddingBottom: 40,
        marginVertical: 10,
        borderRadius: 10, 
        position: 'relative',
    },
    imageWrapper: {alignItems: 'center', marginBottom: 5},
    image: {width: '100%', height: 100, borderRadius: 10},
    title: {fontWeight: 'bold', fontSize: 12, color: '#000'},
    price: {fontSize: 11, marginTop: 3, color: '#000'},
});

const button = StyleSheet.create({
    container: {position: 'absolute', bottom: 12, left: 5, right: 5},
    wrapper: {
        marginTop: 8, 
        backgroundColor: '#9087F4', 
        paddingVertical: 2, 
        paddingHorizontal: 15, 
        borderRadius: 5, 
        elevation: 3
    },
    text: {color: '#fff', fontWeight: 'bold', textAlign: 'center'},
});