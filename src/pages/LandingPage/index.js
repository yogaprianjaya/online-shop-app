import React from "react";
import { Dimensions, Image, Pressable, StyleSheet, Text, View } from "react-native";
import { SanbercodePNG } from "../../Assets/images";

const LandingPage = ({ navigation }) => {
    return(
        <View style={styling.container}>
            <View>
                <Image source={SanbercodePNG} style={styling.image} />
            </View>
            <View style={styling.card}>
                <Pressable style={styling.buttonRegister} onPress={() => navigation.navigate("Register")}>
                    <Text style={styling.textRegister}>Register</Text>
                </Pressable>
                <Pressable style={styling.buttonLogin} onPress={() => navigation.navigate("Login")}>
                    <Text style={styling.textLogin}>Login</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default LandingPage;

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

const styling = StyleSheet.create({
    container: {
        backgroundColor: '#9087F4',
        height: ScreenHeight,
        width: ScreenWidth,
        position: 'relative',
    },
    image: {width: ScreenWidth - 50, height: 350, alignSelf: 'center', marginTop: 90},
    card: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        paddingHorizontal: 30,
        paddingVertical: 35
    },
    buttonRegister: {
        backgroundColor: '#9087F4',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#9087F4',
        paddingVertical: 10,
        marginBottom: 20
    },
    textRegister: {color: '#fff', fontSize: 18, fontWeight: 'bold', textAlign: 'center'},
    buttonLogin: {
        backgroundColor: '#fff',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#9087F4',
        paddingVertical: 10,
    },
    textLogin: {color: '#9087F4', fontSize: 18, fontWeight: 'bold', textAlign: 'center'},
});