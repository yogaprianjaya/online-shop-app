import React from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";

const Categories = (props) => {
    return(
        <ScrollView horizontal showsHorizontalScrollIndicator={false} style={category.container}>
            <View style={category.wrapper}>
                {props.categories.map((item, index) => (
                    <Text style={category.item} key={index}>{item}</Text>
                ))}
            </View>
        </ScrollView>
    );
};

export default Categories;

const category = StyleSheet.create({
    container: {marginTop: 10, marginHorizontal: 5},
    wrapper: {flexDirection: 'row'},
    item: {
        paddingHorizontal: 10, 
        paddingVertical: 5, 
        marginHorizontal: 3, 
        backgroundColor: '#CAF0F8', 
        borderRadius: 10
    }
});