import React, { useEffect, useState } from "react";
import { Image, Pressable, ScrollView, StyleSheet, Text, View } from "react-native";

import { SearchSVG } from '../../../Assets/svg';
import { get } from "../../../Helper/Api";
import Loading from "../../../Components/Loading";


const Products = (props) => {
    const [dataProduct, setDataProduct] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const getDataProduct = async () => {
        const response = await get(`/products/category/${props.categories}?limit=3`);
        if(response) {
            setIsLoading(false);
            setDataProduct(response);
        }
    };

    useEffect(() => {
        getDataProduct();
    }, [dataProduct, isLoading]);

    return(
        <View style={wrapper.container}>
            <Text style={product.categoryName}>Categories {props.categories}</Text>
            {isLoading ? 
                <View style={loading.container}>
                    <Loading />
                </View>
                :
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={product.container}>
                        {dataProduct.map((item, index) => (
                            <View key={index} style={product.wrapperItem}>
                                <View style={product.wrapperImage}>
                                    <Image source={{uri: item.image}} style={product.image} />
                                </View>
                                <Text style={product.title}>{item.title}</Text>
                                <Text style={product.price}>Price : $ {item.price}</Text>
                                <View style={product.wrapperButton}>
                                    <Pressable style={product.button} onPress={() => props.navigation.navigate('ProductDetail', {data: item.id})}>
                                        <Text style={product.buttonText}>Detail Product</Text>
                                    </Pressable>
                                </View>
                            </View>
                        ))}
                        <View style={product.wrapperItem}>
                            <Pressable onPress={() => props.navigation.navigate('ProductCategory', {data: props.categories})}>
                                <View style={product.wrapperSVG}>
                                    <SearchSVG height={140} width={90} />
                                    <Text>Other Items</Text>
                                </View>
                            </Pressable>
                        </View>
                    </View>      
                </ScrollView>
            }
        </View>
    );
};

export default Products;

const wrapper = StyleSheet.create({
    container: {marginHorizontal: 5, marginTop: 5, minHeight: 220},
});

const loading = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

const product = StyleSheet.create({
    categoryName: {fontSize: 16, fontWeight: 'bold', marginBottom: 5, marginLeft: 5, color: '#000'},
    container: {flexDirection: 'row', paddingBottom: 20},
    wrapperItem: {
        backgroundColor: '#fff',
        shadowColor: '#171717',
        elevation: 10,
        borderColor: '#9087F4', 
        borderWidth: 1,
        marginHorizontal: 5, 
        width: 125, 
        paddingHorizontal: 5, 
        paddingTop: 7, 
        paddingBottom: 40,
        borderRadius: 10, 
        position: 'relative',
    },
    wrapperImage: {alignItems: 'center', marginBottom: 5},
    image: {width: '100%', height: 100, borderRadius: 10},
    title: {fontWeight: 'bold', fontSize: 12, color: '#000'},
    price: {fontSize: 11, marginTop: 3, color: '#000'},
    wrapperButton: {position: 'absolute', bottom: 12, left: 5, right: 5},
    button: {
        marginTop: 8, 
        backgroundColor: '#9087F4', 
        paddingVertical: 2, 
        paddingHorizontal: 15, 
        borderRadius: 5, 
        elevation: 3
    },
    buttonText: {color: '#fff', fontWeight: 'bold', textAlign: 'center'},
    wrapperSVG: {alignItems: 'center', justifyContent: 'center'},
});