import React, { useEffect, useState } from "react";
import { ScrollView, StatusBar, StyleSheet, View } from "react-native";

import { Categories, Products } from "./Components";
import { get } from "../../Helper/Api";
import Loading from "../../Components/Loading";

const Home = ({navigation}) => {
    const [categories, setCategories] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const getDataCategories = async () => {
        const response = await get('/products/categories');
        if(response) {
            setIsLoading(false);
            setCategories(response);
        }
    };

    useEffect(() => {
        getDataCategories();
    }, [categories, isLoading]);

    if(isLoading) {
        return(
            <View style={loading.container}>
                <Loading />
            </View>
        )
    }
    return(
        <View style={wrapper.container}>
            <StatusBar backgroundColor="#9087F4" />
            <View style={styling.widthFull}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={wrapper.bodyContent}>
                        <View style={wrapper.wrapperContent}> 
                            {categories.map((item, index) => (
                                <Products key={index} categories={item} navigation={navigation}/>
                            ))}
                        </View>
                    </View>
                </ScrollView>
            </View>
        </View>
    );
};

export default Home;

const styling = StyleSheet.create({
    widthFull: {width: '100%'},
});

const loading = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

const wrapper = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', backgroundColor: '#fff'},
    bodyContent: {
        backgroundColor: '#9087F4',
        paddingTop: 20,
    },
    wrapperContent: {
        width: '100%',
        backgroundColor: '#fff',
        paddingHorizontal: 5,
        paddingTop: 10,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    }
});