import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";

import Loading from "../../Components/Loading";
import { get } from "../../Helper/Api";
import { Cart } from "./Components";

const ProductCategory = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [dataCart, setDataCart] = useState([]);

    const getDataCart = async () => {
        const response = await get(`/carts/user/1`);
        if(response) {
            const dataCart = response.map((item) => {
                const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                const newDate = new Date(item.date);
                const date = months[newDate.getMonth()] + ' ' + newDate.getDate()  + ', ' + newDate.getFullYear();
                return {id: item.id, date: date, products: item.products};
            });
            setDataCart(dataCart);
            setIsLoading(false);
        }
    };

    useEffect(() => {
        getDataCart();
    }, []);

    if(isLoading) {
        return(
            <View style={loading.container}>
                <Loading />
            </View>
        )
    }
    else {
        return (
            <View style={wrapper.container}>
                <View style={styling.widthFull}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={wrapper.body}>
                            <View style={wrapper.content}> 
                                {dataCart.map((item, index) => (
                                    <View key={index} style={cart.container}>
                                        <View style={cart.headerWrapper}>
                                            <Text style={cart.header}>{item.date}</Text>
                                        </View>
                                        {item.products.map((item, index) => (
                                            <Cart key={index} productId={item.productId} quantity={item.quantity} />
                                        ))}
                                    </View>
                                ))}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
};

export default ProductCategory;

const styling = StyleSheet.create({
    widthFull: {width: '100%'},
});

const loading = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
})

const wrapper = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', backgroundColor: '#fff'},
    body: {
        backgroundColor: '#9087F4',
        paddingTop: 20,
    },
    content: {
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
});

const cart = StyleSheet.create({
    container: {marginTop: 20},
    headerWrapper: {backgroundColor: '#9087F4', paddingVertical: 5, paddingHorizontal: 20, borderRadius: 10},
    header: {color: '#fff', fontWeight: 'bold'},
});