import React, {useState, useEffect} from "react";
import { Image, Pressable, StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import Loading from "../../../Components/Loading";
import { get } from "../../../Helper/Api";

const Cart = (props) => {
    const {productId, quantity} = props;
    const [isLoading, setIsLoading] = useState(true);
    const [dataCart, setDataCart] = useState([]); 
    const [dataQuantity, setDataQuantity] = useState(quantity);
    const [dataPayment, setDataPayment] = useState();

    const getDataProduct = async () => {
        const response = await get(`/products/${productId}`);
        if(response) {
            setDataCart({id: response.id, title: response.title, image: response.image, price: response.price});
            setDataPayment(dataQuantity * response.price);
        }
    };

    useEffect(() => {
        getDataProduct();
    }, []);

    const totalPrice = (dataQyt) => {
        const total = dataCart.price * dataQyt;
        setDataPayment(total);
    }

    const addQty = () => {
        const newQyt = dataQuantity + 1;
        setDataQuantity(newQyt);
        totalPrice(newQyt);
    };
    
    const minQty = () => {
        const newQyt = dataQuantity - 1;
        setDataQuantity(newQyt);
        totalPrice(newQyt);
    };

    return(
        <View style={cart.container}>
            <View style={cart.detailWrapper}>
                <Image source={{uri: dataCart.image}} style={cart.detailImage} />
                <Text style={cart.detailTitle}>{dataCart.title}</Text>
            </View>
            <View style={styling.flexColumn}>
                <View style={cart.paymentWrapper}>
                    <Text style={cart.paymentTitle}>Total Payment</Text>
                    <Text style={cart.payment}>$ {dataPayment}</Text>
                </View>
                <View style={cart.quantityWrapper}>
                    <Pressable onPress={minQty}>
                        <Icon name="remove-circle" color='#fff' size={15} />
                    </Pressable>
                    <Text style={cart.quantity}>{dataQuantity}</Text>
                    <Pressable onPress={addQty}>
                        <Icon name="add-circle" color='#fff' size={15} />
                    </Pressable>
                </View>
            </View>
        </View>
    );
};

export default Cart;

const styling = StyleSheet.create({
    flexColumn: {flexDirection: 'column'},
});

const cart = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        paddingVertical: 20,
        borderBottomColor: '#9087F4',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    detailWrapper: {flexDirection: 'row', width: '50%', alignItems: 'center'},
    detailImage: {width: 50, height: 50, borderRadius: 10},
    detailTitle: {marginLeft: 5, fontWeight: 'bold'},
    paymentWrapper: {
        backgroundColor: '#9087F4',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    paymentTitle: {color: '#fff', fontSize: 12, textAlign: 'center'},
    payment: {color: '#fff', fontSize: 15, fontWeight: 'bold', textAlign: 'center'},
    quantityWrapper: {
        backgroundColor: '#9c94f7',
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    quantity: {color: '#fff', fontSize: 15, fontWeight: 'bold', textAlign: 'center'},
});