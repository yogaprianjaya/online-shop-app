import React, { useEffect, useRef, useState } from "react";
import { Image, Pressable, ScrollView, StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import Loading from "../../Components/Loading";
import { get } from "../../Helper/Api";
import { Cart, Description, FlashSale, Images } from "./Components";

const ProductDetail = ({navigation, route}) => {
    const {data} = route.params;
    const [detailProduct, setDetailProduct] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    // Timer References
    const refTimer = useRef();
    
    // For keeping a track on the Timer
    const [timerEnd, setTimerEnd] = useState(false);
    const [timer, setTimer] = useState();
    
    const timerCallbackFunc = (timerFlag) => {
        // Setting timer flag to finished
        setTimerEnd(timerFlag);
    };

    const getDetailProduct = async () => {
        const response = await get(`/products/${data}`);
        if(response) {
            const randomRating = Math.floor(Math.random() * 6);
            let ratingStart = [];
            for (let index = 0; index < randomRating; index++) {
                ratingStart.push(<Icon key={`start-${index}`} name="star" color='gold' size={18} />);
            }
            for (let index = 0; index < 5 - randomRating; index++) {
                ratingStart.push(<Icon key={`outline-start-${index}`} name="star-outline" color='gold' size={18} />);
            }

            const resultData = {
                id: response.id,
                image: response.image,
                description: response.description,
                rating: ratingStart,
                price: response.price,
                data: [
                    {key: 'Title', value: response.title},
                    {key: 'Price', value: `$ ${response.price}`},
                    {key: 'Category', value: response.category},
                ]
            };
            setDetailProduct(resultData);
            setIsLoading(false);
        }
    };

    const getTimer = () => {
        const secondsFinish = 86400;
        const thisTimeSeconds = (new Date().getHours() * 3600) + (new Date().getMinutes() * 60) + new Date().getSeconds();
        setTimer(secondsFinish - thisTimeSeconds);
    }

    useEffect(() => {
        getTimer();
        getDetailProduct();
    }, [data]);

    if(isLoading) {
        return(
            <View style={loading.container}>
                <Loading />
            </View>
        )
    }
    else {
        return (
            <View style={wrapper.container}>
                <View style={styling.widthFull}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={wrapper.body}>
                            <View style={wrapper.content}> 
                                <Images dataImage={detailProduct.image} />

                                {!timerEnd ?
                                    <FlashSale 
                                        dataPrice={detailProduct.price} 
                                        refTimer={refTimer} 
                                        timer={timer} 
                                        timerCallbackFunc={timerCallbackFunc} 
                                    />
                                : null }

                                <Description 
                                    detailData={detailProduct.data} 
                                    detailDescription={detailProduct.description} 
                                    detailRating={detailProduct.rating} 
                                />
                                
                                <Cart idData={detailProduct.id} price={detailProduct.price} />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
};

export default ProductDetail;

const styling = StyleSheet.create({
    widthFull: {width: '100%'},
});

const loading = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', justifyContent: 'center'}
});

const wrapper = StyleSheet.create({
    container: {flex: 1, alignItems: 'center', backgroundColor: '#fff'},
    body: {backgroundColor: '#9087F4', paddingTop: 20},
    content: {
        backgroundColor: '#fff',
        width: '100%',
        paddingHorizontal: 5,
        paddingTop: 10,
        paddingBottom: 20,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
});