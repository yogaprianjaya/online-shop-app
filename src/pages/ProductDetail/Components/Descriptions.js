import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Description = (props) => {
    const {detailData, detailDescription, detailRating} = props;
    return(
        <View style={description.container}>
            {detailData.map((item, index) => (
                <View key={index} style={description.wrapper}>
                    <Text style={description.titleDetail}>{item.key} Product</Text>
                    <Text style={description.detail}>{item.value}</Text>
                </View>
            ))}
            <View style={description.wrapperDescription}>
                <Text style={description.titleDescription}>Description Product</Text>
                <Text style={description.textDescription}>{detailDescription}</Text>
            </View>
            <View style={description.wrapper}>
                <Text style={description.titleDetail}>Rating Product</Text>
                {detailRating}
            </View>
        </View>
    )
};

export default Description;

const description = StyleSheet.create({
    container: {marginTop: 20, marginHorizontal: 30},
    wrapper: {flexDirection: 'row', marginTop: 10, borderBottomWidth: 1, borderBottomColor: '#9087F4', paddingHorizontal: 10, paddingBottom: 10},
    titleDetail: {fontWeight: 'bold', fontSize: 16, width: '45%'},
    detail: {fontSize: 16, width: '55%'},
    wrapperDescription: {marginTop: 10, borderBottomWidth: 1, borderBottomColor: '#9087F4', paddingHorizontal: 10, paddingBottom: 10},
    titleDescription: {fontWeight: 'bold', fontSize: 16, textAlign: 'center', marginBottom: 10},
    textDescription: {fontSize: 16, textAlign: 'justify'},
});