import React from "react";
import { Image, StyleSheet, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

const Images = (props) => {
    const {dataImage} = props;
    return(
        <View style={image.wrapper}>
            <Icon name="chevron-back" size={50} />
            <Image source={{uri: dataImage}} style={image.image} />
            <Icon name="chevron-forward" size={50} />
        </View>
    );
};

export default Images;

const image = StyleSheet.create({
    wrapper: {marginTop: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly'},
    image: {width: 200, height: 200, borderRadius: 20, borderWidth: 4, borderColor: '#9c94f7'},
})