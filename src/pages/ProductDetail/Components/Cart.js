import React, {useState} from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

const Cart = (props) => {
    const {id, price} = props;
    const [buttonCart, setButtonCart] = useState(false);
    const [qytCart, setQytCart] = useState(0);
    const [payment, setPayment] = useState(0);

    const toogleButton = () => {
        if(buttonCart) {
            setQytCart(0);
        }
        setButtonCart(!buttonCart);
    };

    const totalPrice = (dataQyt) => {
        const total = price * dataQyt;
        setPayment(total)
    }

    const addQty = () => {
        const newQyt = qytCart + 1;
        setQytCart(newQyt);
        totalPrice(newQyt);
    };
    
    const minQty = () => {
        const newQyt = qytCart - 1;
        if(newQyt == 0) {setButtonCart(false)}
        setQytCart(newQyt);
        totalPrice(newQyt);
    };

    return(
        <View style={wrapper.container}>
            <View style={cart.container}>
                <View style={cart.headerWrapper}>
                    <Icon name="cart-outline" color='#fff' size={13} />
                    <Text style={cart.header}>your cart of this product</Text>
                </View>
                <View style={cart.bodyWrapper}>
                    {buttonCart ? 
                        <View style={styling.widthFull}>
                            <View style={styling.flexBetween}>
                                <View style={styling.flexColumn}>
                                    <Text style={cart.textBody}>Total Payment</Text>
                                    <Text style={cart.payment}>$ {payment}</Text>
                                </View>
                                <View style={styling.flexRow}>
                                    <Pressable onPress={minQty}>
                                        <Icon name="remove-circle" color='#fff' size={20} />
                                    </Pressable>
                                    <Text style={cart.qytItem}>{qytCart}</Text>
                                    <Pressable onPress={addQty}>
                                        <Icon name="add-circle" color='#fff' size={20} />
                                    </Pressable>
                                </View>
                            </View>
                        </View>
                    :
                        <View>
                            <Text style={cart.textBody}>You haven't added this item to your cart yet</Text>
                        </View>
                    }
                </View>
            </View>
            <View style={button.container}>
                <Pressable onPress={toogleButton}>
                    <View style={button.wrapper}>
                        <Text style={button.text}>{buttonCart ? 'Remove from' : 'Add to'} cart</Text>
                    </View>
                </Pressable>
            </View>
        </View>
    );
};

export default Cart;

const styling = StyleSheet.create({
    widthFull: {width: '100%'},
    flexBetween: {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'},
    flexColumn: {flexDirection: 'column'},
    flexRow: {flexDirection: 'row'},
});

const wrapper = StyleSheet.create({
    container: {flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 5, marginTop: 20},
});

const cart = StyleSheet.create({
    container: {width: '70%', justifyContent: 'center'},
    headerWrapper: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        paddingVertical: 3,
        backgroundColor: '#9087F4', 
        borderTopRightRadius: 15, 
        borderTopLeftRadius: 15,
    },
    header: {color: '#fff', fontSize: 13, fontWeight: 'bold', marginLeft: 5},
    bodyWrapper: {
        backgroundColor: '#9c94f7', 
        borderBottomRightRadius: 15, 
        borderBottomLeftRadius: 15, 
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    textBody: {color: '#fff', textAlign: 'center', fontSize: 12},
    payment: {color: '#fff', textAlign: 'center', fontSize: 15, fontWeight: 'bold'},
    qytItem: {color: '#fff', marginHorizontal: 10, fontSize: 20, fontWeight: 'bold'},
});

const button = StyleSheet.create({
    container: {width: '25%', justifyContent: 'center'},
    wrapper: {
        backgroundColor: '#9087F4', 
        paddingVertical: 10, 
        paddingHorizontal: 10, 
        borderRadius: 5, 
        elevation: 10
    },
    text: {color: '#fff', fontWeight: 'bold', textAlign: 'center'},
});