import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import CountDownTimer from 'react-native-countdown-timer-hooks';

const FlashSale = (props) => {
    const {dataPrice, refTimer, timer, timerCallbackFunc} = props;
    return(
        <View style={flashSale.container}>
            <View style={flashSale.headerWrapper}>
                <Icon name="flash-outline" color='#fff' size={16} />
                <Text style={flashSale.header}>Flash Sale</Text>
            </View>
            <View style={flashSale.body}>
                <View style={styling.flexColumnCenter}>
                    <View style={styling.flexRowCenter}>
                        <Text style={flashSale.soldOutQty}>+ {Math.floor(Math.random() * 100)}</Text>
                        <Text style={flashSale.soldOut}>Sold</Text>
                    </View>
                    <View style={styling.flexRowCenter}>
                        <Text style={flashSale.discountPrice}>$ {dataPrice}</Text>
                        <Text style={flashSale.normalPrice}>{((100/70)*dataPrice).toFixed(3)}</Text>
                    </View>
                </View>
                <View style={styling.flexColumnCenter}>
                    <Text style={flashSale.countDownText}>Ends In</Text>
                    <CountDownTimer
                        ref={refTimer}
                        timestamp={timer}
                        timerCallback={timerCallbackFunc}
                        textStyle={flashSale.countDown}
                    />
                </View>
            </View>
        </View>
    );
};

export default FlashSale;

const styling = StyleSheet.create({
    flexColumnCenter: {flexDirection: 'column', alignItems: 'center'},
    flexRowCenter: {flexDirection: 'row', alignItems: 'center'},
});

const flashSale = StyleSheet.create({
    container: {
        backgroundColor: '#9087F4', 
        marginTop: 20, 
        marginHorizontal: 5, 
        borderRadius: 15, 
        shadowColor: '#171717', 
        elevation: 10
    },
    headerWrapper: {flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 3},
    header: {color: '#fff', fontSize: 16, fontWeight: 'bold', marginLeft: 5},
    body: {
        backgroundColor: '#9c94f7', 
        borderBottomRightRadius: 15, 
        borderBottomLeftRadius: 15, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        paddingVertical: 3, 
        paddingHorizontal: 15
    },
    soldOutQty: {color: '#fff', fontSize: 15, fontWeight: 'bold', marginLeft: 5},
    soldOut: {color: '#fff', fontSize: 13, marginLeft: 5},
    discountPrice: {color: '#fff', fontSize: 18, fontWeight: 'bold', marginLeft: 5},
    normalPrice: {color: '#fff', fontSize: 12, marginLeft: 5, textDecorationLine: 'line-through'},
    countDownText: {color: '#fff', fontSize: 15, textAlign: 'center', marginRight: 5},
    countDown: {fontSize: 20, color: '#FFFFFF', fontWeight: '500', letterSpacing: 2.5},
});