export { default as Account } from './Account';
export { default as Cart } from './Cart';
export { default as Home } from './Home';
export { default as LandingPage } from './LandingPage';
export { default as Login } from './Login';
export { default as ProductCategory } from './ProductCategory';
export { default as ProductDetail } from './ProductDetail';
export { default as Register } from './Register';