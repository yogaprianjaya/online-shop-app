import React, { useContext, useState } from "react";
import { Dimensions, Image, Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import { SanbercodePNG } from "../../Assets/images";
import { post } from "../../Helper/Api";
import { AuthContext } from "../../Context/AuthContext";

const Login = () => {
    const [input, setInput] = useState({username: '', password: ''});
    const [token, setToken] = useContext(AuthContext);
    const HandlerLogin = async () => {
        const body = {
            username: input.username,
            password: input.password
        }
        const header = {
            headers: {'content-type': 'application/json'}
        }
        const response = await post('/auth/login', body, header);
        console.log(response)
        if(response) {
            if(response.token) {
                setToken(response.token);
            }
        }
    };
    return(
        <View style={styling.container}>
            <View>
                <Image source={SanbercodePNG} style={styling.image} />
            </View>
            <View style={styling.card}>
                <Text style={styling.title}>Sign in</Text>
                <View style={styling.formInput}>
                    <TextInput placeholder="Username" style={styling.input} defaultValue={input.username} onChangeText={(text) => setInput({...input, username: text})}/>
                    <TextInput placeholder="Password" style={styling.input} defaultValue={input.password} onChangeText={(text) => setInput({...input, password: text})}/>
                </View>
                <View style={styling.footer}>
                    <Text style={styling.textQuestion}>Forgot Password ?</Text>
                    <Pressable onPress={HandlerLogin} style={styling.buttonSubmit}>
                        <Text style={styling.textSignIn}>Sign In</Text>
                    </Pressable>
                </View>
            </View>
        </View>
    )
}

export default Login;

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

const styling = StyleSheet.create({
    container: {
        backgroundColor: '#9087F4',
        height: ScreenHeight,
        width: ScreenWidth,
        position: 'relative',
    },
    image: {width: ScreenWidth - 50, height: 250, alignSelf: 'center', marginTop: 90},
    card: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        paddingHorizontal: 30,
        paddingVertical: 35
    },
    title: { fontWeight: 'bold', fontSize: 45, color: '#000' },
    formInput: { paddingVertical: 20 },
    input: { borderBottomWidth: 1, borderBottomColor: '#828282', fontSize: 18, marginBottom: 10 },
    footer: {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 20},
    textQuestion: {color: '#9087F4', fontWeight: 'bold'},
    textSignIn: {color: '#fff', fontWeight: 'bold'},
    buttonSubmit: {paddingVertical: 8, paddingHorizontal: 25, borderRadius: 8, backgroundColor: '#9087F4'}
});