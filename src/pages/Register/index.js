import React, { useState } from "react";
import { Dimensions, Image, Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import { SanbercodePNG } from "../../Assets/images";
import { post } from "../../Helper/Api";

const Register = ({ navigation }) => {
    const [input, setInput] = useState({name: '', email: '', phone: '', password: ''});
    const HandlerRegist =  async () => {
        const body = {
            email: input.email,
            username: input.name.trim(),
            password: input.password.trim(),
            name: {
                firstname: input.name,
                lastname: '',
            },
            address: {
                city:'kilcoole',
                street:'7835 new road',
                number:3,
                zipcode:'12926-3874',
                geolocation:{
                    lat:'-37.3159',
                    long:'81.1496'
                }
            },
            phone: input.phone
        }
        const header = {
            headers: {'content-type': 'application/json'}
        }
        const response = await post('/users', body, header);
        if(response) {
            navigation.navigate('Login');
        }
    };
    return(
        <View style={styling.container}>
            <View>
                <Image source={SanbercodePNG} style={styling.image} />
            </View>
            <View style={styling.card}>
                <Text style={styling.title}>Sign Up</Text>
                <View style={styling.formInput}>
                    <TextInput placeholder="Name" style={styling.input} defaultValue={input.name} onChangeText={(text) => setInput({...input, name: text})}/>
                    <TextInput placeholder="Email" style={styling.input} defaultValue={input.email} onChangeText={(text) => setInput({...input, email: text})}/>
                    <TextInput placeholder="Phone Number" style={styling.input} defaultValue={input.phone} onChangeText={(text) => setInput({...input, phone: text})}/>
                    <TextInput placeholder="Password" style={styling.input} defaultValue={input.password} onChangeText={(text) => setInput({...input, password: text})}/>
                </View>
                <View style={styling.footer}>
                    <Text style={styling.textQuestion}>Already have an account ?</Text>
                    <Pressable onPress={HandlerRegist} style={styling.buttonSubmit}>
                        <Text style={styling.textSignIn}>Sign Up</Text>
                    </Pressable>
                </View>
            </View>
        </View>
    )
}

export default Register;

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

const styling = StyleSheet.create({
    container: {
        backgroundColor: '#9087F4',
        height: ScreenHeight,
        width: ScreenWidth,
        position: 'relative',
    },
    image: {width: ScreenWidth - 50, height: 150, alignSelf: 'center', marginTop: 90},
    card: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        paddingHorizontal: 30,
        paddingVertical: 35
    },
    title: { fontWeight: 'bold', fontSize: 45, color: '#000' },
    formInput: { paddingVertical: 20 },
    input: { borderBottomWidth: 1, borderBottomColor: '#828282', fontSize: 18, marginBottom: 10 },
    footer: {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 20},
    textQuestion: {color: '#9087F4', fontWeight: 'bold'},
    textSignIn: {color: '#fff', fontWeight: 'bold'},
    buttonSubmit: {paddingVertical: 8, paddingHorizontal: 25, borderRadius: 8, backgroundColor: '#9087F4'}
});