import React from "react";
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { Home, ProductCategory, ProductDetail } from "../pages";

const Stack = createNativeStackNavigator();

const HomeStack = () => {
    return(
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="HomeScreen" component={Home} />
            <Stack.Screen name="ProductCategory" component={ProductCategory} />
            <Stack.Screen name="ProductDetail" component={ProductDetail} />
        </Stack.Navigator>
    );
}

export default HomeStack;