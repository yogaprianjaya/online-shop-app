import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Account, Cart } from '../pages';
import Icon from 'react-native-vector-icons/Ionicons';
import HomeStack from './HomeStack';

const Tab = createBottomTabNavigator();

const TabNavigation = () => {
    return(
        <Tab.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: '#9087F4',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    fontSize: 20,
                },
                headerTitleAlign: 'center',
                headerShadowVisible: false,
            }}
        >
            <Tab.Screen name="Home" component={HomeStack} 
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="home-outline" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Cart" component={Cart} 
                options={{
                    tabBarLabel: 'Cart',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="cart-outline" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Account" component={Account} 
                options={{
                    tabBarLabel: 'Account',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="person-outline" color={color} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}

export default TabNavigation;