import React, { useContext } from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { LandingPage, Login, Register } from '../pages';
import TabNavigation from './TabNavigation';
import { AuthContext } from '../Context/AuthContext';

const Stack = createStackNavigator();

const StackNavigation = () => {
    const [token] = useContext(AuthContext);
    return(
        <>
            {token != null ?
                <Stack.Navigator screenOptions={{headerShown: false}} >
                    <Stack.Screen name="HomePage" component={TabNavigation} />
                </Stack.Navigator>
                :
                <Stack.Navigator initialRouteName="LandingPage" >
                    <Stack.Screen name="LandingPage" component={LandingPage} options={{ headerShown: false }} />
                    <Stack.Screen name="Login" component={Login} options={{ headerTransparent: true, headerTitle: '' }} />
                    <Stack.Screen name="Register" component={Register} options={{ headerTransparent: true, headerTitle: ''}} />
                </Stack.Navigator>
            }
        </>
    )
}

export default StackNavigation;