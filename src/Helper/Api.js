import React from "react";
import axios from 'axios';

const baseUrl = 'https://fakestoreapi.com';

const axiosApi = axios.create({
    baseURL: baseUrl,
});

export async function get(url, config = {}) {
    return await axiosApi.get(url, { ...config }).then(response => response.data);
};

export async function post(url, data, config = {}) {
    return await axiosApi.post(url, data, { ...config }).then(response => response.data);
};