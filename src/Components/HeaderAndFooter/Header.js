import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Header = () => {
    return(
        <View style={styling.wrapper}>
            <Text style={styling.title}>Online Shop App</Text>
            <Text style={styling.subTitle}>by. yoga prianjaya</Text>
        </View>
    );
};

export default Header;

const styling = StyleSheet.create({
    wrapper: {
        backgroundColor: '#0096C7', 
        padding: 20,
        height: 80
    },
    title: {textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: '#CAF0F8'},
    subTitle: {textAlign: 'center', fontSize: 12, color: '#CAF0F8'}
});