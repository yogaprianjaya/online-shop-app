import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { AccountPNG, CartPNG, HomePNG } from "../../Assets/images";

const Footer = () => {
    return(
        <View style={styling.wrapper}>
            <View style={styling.wrapperMenu}>
                <Image source={HomePNG} style={styling.imageMenu} />
                <Text style={styling.textMenu}>Home</Text>
            </View>
            <View style={styling.wrapperMenu}>
                <Image source={CartPNG} style={styling.imageMenu} />
                <Text style={styling.textMenu}>Cart</Text>
            </View>
            <View style={styling.wrapperMenu}>
                <Image source={AccountPNG} style={styling.imageMenu} />
                <Text style={styling.textMenu}>Account</Text>
            </View>
        </View>
    );
};

export default Footer;

const styling = StyleSheet.create({
    wrapper: {
        height: 60,
        backgroundColor: '#0096C7', 
        padding: 10, 
        borderTopLeftRadius: 30, 
        borderTopRightRadius: 30, 
        flexDirection: 'row', 
        justifyContent: 'space-evenly', 
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    },
    wrapperMenu: {alignItems: 'center'},
    imageMenu: {height: 20, width: 20},
    textMenu: {color: '#fff', fontSize: 13}
});